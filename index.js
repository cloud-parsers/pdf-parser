const fs = require('fs');
const path = require('path');
const express = require('express');
const pdf = require('pdf-parse');
const cors = require('cors');
const dotenv = require('dotenv');

dotenv.config();

const app = express();

const staticDir = process.env.PDF_FILES_PATH || path.resolve(__dirname, 'static');

app.use(cors());

const makeFileData = ([fileName,founded_line]) => ({
  url: `${process.env.FILE_SERVER_ADDRESS}/${fileName}`,
  name: fileName,
  founded_line: founded_line
});

const copyToTempFolder = async (fileName) => {
  return fs.promises.copyFile(
    path.join(staticDir, fileName),
    path.join(process.env.FS_TEMP_FOLDER, fileName)
  );
}

app.get('/parse', async (req, res) => {
  const query = req.query.query?.toLowerCase();

  if (!query) {
    res.status(400).end('"query" param is missing');
    return;
  }

  try {
    const fileNames = await fs.promises.readdir(staticDir, 'utf-8');
    const filteredFiles = [];

    await Promise.all(
      fileNames.map(async (fileName) => {
        if (!fileName.endsWith('.pdf')) {
          return;
        }

        if (fileName.toLowerCase().includes(query)) {
          filteredFiles.push([fileName,null]);
          await copyToTempFolder(fileName);
          return;
        }

        const filePath = path.resolve(staticDir, fileName);
        const buffer = await fs.promises.readFile(filePath);

        const data = await pdf(buffer);

        if (data.text.toLowerCase().includes(query)) {
			lines=data.text.split("\n");
			lineIndex=lines.findIndex(line => line.toLowerCase().includes(query));
			founded_line=null;
			if (lineIndex !== -1) 
				founded_line=lines[lineIndex];
          filteredFiles.push([fileName, founded_line]);
          await copyToTempFolder(fileName);
        }
      })
    );

    const responseText = JSON.stringify(
      { files: filteredFiles.map(makeFileData) },
      null,
      2
    );

    res.header('Content-Type', 'application/json');
    res.status(200).end(responseText);
  } catch (err) {
    console.error(err);
    res.status(500).end('Something went wrong');
  }
});

const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`PDF parser listening request on port ${port}`);
});
