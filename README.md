This is repo for parser web-server

To run use `node index.js` or`docker-compose up parser1`

API:

`GET: /parse`, params: `query` - query to find text in files and their names.
Returns JSON object in next format: `{ files: [{url, name}...] }`

[TODO](https://trello.com/b/Cy5IJhz6/pdf-parser):
- Add files source
